# Docker worker planner

## Exercice 1 : 

Dans les fichiers planner et worker :

    npm install
    npm audit fix --force


(dans planner/main.js) decommenter ligne 16 :

    { url: 'http://localhost:8080', id: '' }

    
- changer le nombre de taches de 20 a 4 dans la ligne 6
    
Pour faire tourner en local :

* dans worker et planner executer : ``node main ``

**faire tourner worker en premier puis le planner**

- créé les deux Dockerfile pour planner et worker :

Dockerfile.planner :
```
FROM node:18.13.0

WORKDIR /app

COPY package.json ./package.json

COPY package-lock.json ./package-lock.json

RUN npm install

COPY . .

EXPOSE 3000

ENV TASKS=4

CMD [ "node", "main"]
```

Dockerfile.worker :
```
FROM node:18.13.0

WORKDIR /app

COPY package.json ./package.json

COPY package-lock.json ./package-lock.json

RUN npm install

COPY . .

ENV MULT=true \
    ADD=true

EXPOSE 8080

CMD [ "node", "main"]
```
(dans planner/main.js)
        changer la ligne 16 en : 
        
        { url: 'http://worker:8080', id: '' }

- crée le buildDocker.sh et le faire tourner (.bat pour windows et l'executer avec : .\buildDocker.bat) :
    ```
    cd worker
    docker build -t worker . -f Dockerfile.worker

    cd ..

    cd planner
    docker build -t planner . -f Dockerfile.planner
    ```
    
**Ou bien executer directement les deux commandes (avec cd pour etre dans le bon dossier: worker/planner)

- crée son réseau : (startDocker.bat)
```
    docker network rm mynetwork
    docker network create mynetwork
    docker run --network=mynetwork --name worker -p 8080:8080 -d worker
    docker run --network=mynetwork --name planner -p 3000:3000 -d planner
```

- pour tout arreter : executer le fichier stopDocker.bat :
```
docker stop worker worker1 planner

docker rm worker worker1 planner

docker image prune -a
```

## Exercice 2 : 

- Modifier le fichier le planner/main.js : 

    - A la ligne : 
    ```
    let workers = { url: 'http://worker:8080', id: '' }
    ```
    - remplacer par :

    ```
    let workers = [ 
        { url: 'http://worker:8080/', id: '0' },
        { url: 'http://worker1:8070/', id: '1' }
    ]
    ```

- Executer le stopDocker.bat (.\stopDocker.bat)
- Executer buildDocker.bat (rebuild) (.\buildDocker.bat)
- Executer le startDocker.bat (.\stopDocker.bat)

## Exercice 3 : 

- Modifier le fichier planner main.js :
    - A la ligne 16 :
    ```
    let workers = [
        { url: 'http://worker:8080', id: '0', type: 'mult' },
        { url: 'http://worker1:8070', id: '1', type: 'add' }
    ]

    let multWorkers = workers.filter((w) => w.type == 'mult');
    let addWorkers = workers.filter((w) => w.type == 'add');
    ```

    - Modifier la ligne : 
    ```
    workers = workers.filter((w) => w.id !== worker.id)
    ```
    - et la remplacer par :
    ```
    if (worker.type == 'mult'){
        multWorkers = multWorkers.filter((w) => w.id !== worker.id)
    }

    if (worker.type == 'add'){
        addWorkers = addWorkers.filter((w) => w.id !== worker.id)
    }
    ```
    - Modifier les lignes :
    ```
    .then((res) => {
        workers = [...workers, worker]
        return res.json()
    })
    ```
    - Par :
    ```
    .then((res) => {
        if (worker.type == 'mult'){
            multWorkers = [...multWorkers, worker]
        }

        if (worker.type == 'add'){
            addWorkers = [...addWorkers, worker]
        }
        return res.json()
    })
    ```
    - Et modifier le const main = async () => { ... } par :
    ```
    const main = async () => {
    console.log(tasks)
    console.log("workers : ",workers)
    console.log("multworkers : ",multWorkers)
    console.log("addworkers : ",addWorkers)

    while (taskToDo > 0) {
        await wait(100)
        if (multWorkers.length === 0 || addWorkers.length === 0 || tasks.length === 0) continue

    if(tasks[0].type == 'mult')
    {
    multWorkers.length == 0 ? console.log("No worker available") : sendTask(multWorkers[0], tasks[0]) 
    continue
    }
    if(tasks[0].type == 'add' )
    {
        multWorkers.length == 0 ? tconsole.log("No worker available") : sendTask(addWorkers[0], tasks[0])
        continue
    }
    continue
    }
    console.log('end of tasks')
    server.close()
    }
    ```
- Executer le stopDocker.bat (.\stopDocker.bat)
- Executer buildDocker.bat (rebuild) (.\buildDocker.bat)
- Executer le startDocker.bat (.\stopDocker.bat)

## Exercice 4 : 

- crée le docker-composer.yaml :


    ```
    version: '3.8'

    services:
    planner:
        build:
        context: ./planner
        dockerfile: DockerFile.planner
        ports:
        - "3000:3000"
        networks:
        - mynetwork

    worker:
        build:
        context: ./worker
        dockerfile: Dockerfile.worker
        ports:
        - "8080:8080"
        networks:
        - mynetwork

    worker1:
        build:
        context: ./worker
        dockerfile: Dockerfile.worker
        environment:
        - PORT=8070
        networks:
        - mynetwork
        deploy:
        replicas: 5

    networks:
    mynetwork:
        driver: bridge
    ```

- Et le faire tourner avec la commande : ``docker-compose up`` 

- modifier le main de planner et remplacer :

```
let workers = [
    { url: 'http://worker:8080', id: '0', type: 'mult' },
    { url: 'http://worker1:8070', id: '1', type: 'add' }
]
```

- par : 

```
let workers = []

for (let i = 1; i <= 10; i++) {
  const workerType = Math.random() < 0.5 ? 'mult' : 'add';
      
  const worker = {
    url: "http://docker-planner-worker-worker-"+ i + ":8080",
    id: i.toString(),
    type: workerType,
  };
  workers.push(worker); 
}
```

- modifier le docker-composer.yaml :
```
version: '3.8'

services:
  planner:
    build:
      context: ./planner
      dockerfile: Dockerfile.planner
    depends_on:
      - worker
    ports:
      - "3000:3000"
    networks:
      - mynetwork

  worker:
    build:
      context: ./worker
      dockerfile: Dockerfile.worker
    networks:
      - mynetwork
    deploy:
      replicas: 10

networks:
  mynetwork:
    driver: bridge
```

- Et le lancer avec la commande : ``docker-compose up -d``

## Exercice 5 :

- 
